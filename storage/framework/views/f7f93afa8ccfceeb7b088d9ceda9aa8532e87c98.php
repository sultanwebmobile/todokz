<div class="row">
    <div class="col-md-8">
        <?php echo e(Form::text('name', trans('tag::attributes.name'), $errors, $tag, ['required' => true])); ?>


        <?php if($tag->exists): ?>
            <?php echo e(Form::text('slug', trans('tag::attributes.slug'), $errors, $tag, ['required' => true])); ?>

        <?php endif; ?>
    </div>
</div>
<?php /**PATH /var/www/todokz/Modules/Tag/Resources/views/admin/tags/tabs/general.blade.php ENDPATH**/ ?>