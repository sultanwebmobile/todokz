<div class="row">
    <div class="col-md-8">
        <?php echo e(Form::number('rate', trans('currency::attributes.rate'), $errors, $currencyRate, ['required' => true])); ?>

    </div>
</div>
<?php /**PATH /var/www/todokz/Modules/Currency/Resources/views/admin/currency_rates/tabs/general.blade.php ENDPATH**/ ?>