<?php $__env->startComponent('admin::components.page.header'); ?>
    <?php $__env->slot('title', trans('review::reviews.reviews')); ?>

    <li class="active"><?php echo e(trans('review::reviews.reviews')); ?></li>
<?php if (isset($__componentOriginald1838d9cbc3e98f76d88606a9883c1cec482d537)): ?>
<?php $component = $__componentOriginald1838d9cbc3e98f76d88606a9883c1cec482d537; ?>
<?php unset($__componentOriginald1838d9cbc3e98f76d88606a9883c1cec482d537); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('admin::components.page.index_table'); ?>
    <?php $__env->slot('resource', 'reviews'); ?>
    <?php $__env->slot('name', trans('review::reviews.review')); ?>

    <?php $__env->slot('thead'); ?>
        <tr>
            <?php echo $__env->make('admin::partials.table.select_all', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <th><?php echo e(trans('admin::admin.table.id')); ?></th>
            <th><?php echo e(trans('review::reviews.table.product')); ?></th>
            <th><?php echo e(trans('review::reviews.table.reviewer_name')); ?></th>
            <th><?php echo e(trans('review::reviews.table.rating')); ?></th>
            <th><?php echo e(trans('review::reviews.table.approved')); ?></th>
            <th data-sort><?php echo e(trans('admin::admin.table.date')); ?></th>
        </tr>
    <?php $__env->endSlot(); ?>
<?php if (isset($__componentOriginalc2e763454f271710e588c202de697a849434b70e)): ?>
<?php $component = $__componentOriginalc2e763454f271710e588c202de697a849434b70e; ?>
<?php unset($__componentOriginalc2e763454f271710e588c202de697a849434b70e); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

<?php $__env->startPush('scripts'); ?>
    <script>
        new DataTable('#reviews-table .table', {
            columns: [
                { data: 'checkbox', orderable: false, searchable: false, width: '3%' },
                { data: 'id', width: '5%' },
                { data: 'product', name: 'product.price', orderable: false, searchable: false, defaultContent: '' },
                { data: 'reviewer_name' },
                { data: 'rating' },
                { data: 'status', name: 'is_approved', searchable: false },
                { data: 'created', name: 'created_at' },
            ],
        });
    </script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('admin::layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/todokz/Modules/Review/Resources/views/admin/reviews/index.blade.php ENDPATH**/ ?>