<?php $__env->startPush('shortcuts'); ?>
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd><?php echo e(trans('admin::admin.shortcuts.back_to_index', ['name' => trans('tag::tags.tag')])); ?></dd>
    </dl>
<?php $__env->stopPush(); ?>

<?php $__env->startPush('scripts'); ?>
    <script>
        keypressAction([
            { key: 'b', route: "<?php echo e(route('admin.tags.index')); ?>" }
        ]);
    </script>
<?php $__env->stopPush(); ?>
<?php /**PATH /var/www/todokz/Modules/Tag/Resources/views/admin/tags/partials/shortcuts.blade.php ENDPATH**/ ?>