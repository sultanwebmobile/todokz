<?php

namespace TodoKZ;

class TodoKZ
{
    /**
     * The TodoKZ version.
     *
     * @var string
     */
    const VERSION = '1.0.0';

    /**
     * The envato item ID.
     *
     * @var string
     */
    const ITEM_ID = '1';
}
