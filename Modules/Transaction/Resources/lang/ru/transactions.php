<?php

return [
    'transactions' => 'Транзакции',
    'table' => [
        'order_id' => 'ID заказа',
        'transaction_id' => 'ID транзакции',
        'payment_method' => 'Способ оплаты',
    ],
];
