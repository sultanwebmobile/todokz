<?php

return [
    'billing' => [
        'first_name' => 'Имя',
        'last_name' => 'Фамилия',
        'address_1' => 'Адресная строка 1',
        'address_2' => 'Адресная строка 2 (необязательно)',
        'city' => 'Город',
        'zip' => 'Почтовый индекс',
        'country' => 'Страна',
        'state' => 'Область/провинция',
    ],
    'shipping' => [
        'first_name' => 'Имяe',
        'last_name' => 'Фамилия',
        'address_1' => 'Адресная строка 1',
        'address_2' => 'Адресная строка 2 (необязательно)',
        'city' => 'Город',
        'zip' => 'Почтовый индекс',
        'country' => 'Country',
        'state' => 'Область/провинция',
    ],
    'street_address' => 'Адрес улицы',
    'customer_email' => 'Эл. адрес',
    'customer_phone' => 'Телефон',
    'create_an_account' => 'Создать аккаунт?',
    'password' => 'Пароль',
    'ship_to_a_different_address' => 'Доставить по другому адресу',
    'order_note' => 'Примечание заказа',
];
