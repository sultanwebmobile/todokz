<?php

return [
    'invoice' => 'Счет-фактура',
    'invoice_id' => 'Идентификатор счета',
    'date' => 'Дата',
    'order_details' => 'Информация для заказа',
    'email' => 'Эл. адрес',
    'phone' => 'Телефон',
    'shipping_method' => 'Способ доставки',
    'payment_method' => 'Метод оплаты',
    'billing_address' => 'Платежный адрес',
    'shipping_address' => 'Адрес доставки',
    'product' => 'Продукт',
    'unit_price' => 'Цена за единицу',
    'quantity' => 'Количество',
    'line_total' => 'Итого по строке',
    'subtotal' => 'Промежуточный итог',
    'total' => 'Итого',
];
