<?php

return [
    'canceled' => 'Отменено',
    'completed' => 'Завершено',
    'on_hold' => 'На удерживании',
    'pending' => 'В ожидании',
    'pending_payment' => 'Незавершенный платеж',
    'processing' => 'Обработывается',
    'refunded' => 'Возвращено',
];
