<?php

return [
    'your_order_status_changed_subject' => 'Статус вашего заказа изменен',
    'your_order_status_changed_text' => 'Ваш заказ #:order_id статус изменен на :status.',
];
