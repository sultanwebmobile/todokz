

<?php

return [
    'tag' => 'Тег',
    'tags' => 'Теги',
    'table' => [
        'name' => 'Название',
    ],
    'tabs' => [
        'group' => [
            'tag_information' => 'Информация о теге',
        ],
        'general' => 'Общее',
    ],
];
