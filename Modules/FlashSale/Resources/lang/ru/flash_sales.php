<?php

return [
    'flash_sale' => 'Флэш-продажа',
    'flash_sales' => 'Флэш-продажи',
    'table' => [
        'campaign_name' => 'Название кампании',
    ],
    'tabs' => [
        'group' => [
            'flash_sale_information' => 'Информация о продаже Flash',
        ],
        'products' => 'Товары',
        'settings' => 'Настройки',
    ],
    'form' => [
        'add_product' => 'Добавить товар',
        'flash_sale_product' => 'Продукт Флэш-продажи',
    ],
];
