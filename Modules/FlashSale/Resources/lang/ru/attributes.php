<?php

return [
    'product' => 'Продукт',
    'end_date' => 'Дата окончания',
    'price' => 'Цена',
    'quantity' => 'Количетсво',
    'campaign_name' => 'Campaign Name',

    // validation
    'products.*.product_id' => 'Продукт',
    'products.*.end_date' => 'Дата окончания',
    'products.*.price' => 'Цена',
    'products.*.qty' => 'Количество',
];
