<?php

return [
    'media' => 'Медиа',
    'drop_files_here' => 'Перепасть файлы здесь или нажмите, чтобы загрузить',
    'upload_new_file' => 'Загрузите новый файл',
    'browse' => 'Просматривать',
    'thumbnails' => 'миниатюры',
    'table' => [
        'thumbnail' => 'миниатюра',
        'filename' => 'Имя файла',
        'width' => 'Ширина',
        'height' => 'Высота',
    ],
    'file_manager' => [
        'title' => 'Файловый менеджер',
        'select_this_file' => 'Выберите этот файл',
    ],
];
