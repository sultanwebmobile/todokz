<?php

return [
    'user' => 'Пользователь',
    'users' => 'Пользователи',
    'profile' => 'Профиль',
    'table' => [
        'first_name' => 'Имя',
        'last_name' => 'Фамилия',
        'email' => 'Эл. адрес',
        'last_login' => 'Последний вход',
    ],
    'tabs' => [
        'group' => [
            'user_information' => 'информация о пользователе',
            'profile_information' => 'Сведения о профиле',
        ],
        'account' => 'Аккаунт',
        'permissions' => 'Разрешения',
        'new_password' => 'Новый парольd',
    ],
    'form' => [
        'activated' => 'Активирован',
    ],
    'or_reset_password' => 'или сбросить парольd',
    'send_reset_password_email' => 'Отправить пароль по электронной почте',
];
