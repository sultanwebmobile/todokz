<?php

return [
    'auth' => [
        'remember_me' => 'Запомнить меня',
    ],

    'users' => [
        'first_name' => 'Имя',
        'last_name' => 'Фамилия',
        'email' => 'Эл. адрес',
        'phone' => 'Телефон',
        'password' => 'Пароль',
        'password_confirmation' => 'Подтвердить Пароль',
        'captcha' => 'Captcha',
        'roles' => 'Роли',
        'activated' => 'Статус',
        'new_password' => 'Новый парольd',
        'confirm_new_password' => 'Подтвердите новый пароль',
    ],

    'roles' => [
        'name' => 'Имя',
    ],
];
