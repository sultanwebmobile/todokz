<?php

return [
    'role' => 'Роль',
    'roles' => 'Роли',
    'table' => [
        'name' => 'Имя',
    ],
    'tabs' => [
        'role_information' => 'Информация о роли',
        'general' => 'Общее',
        'permissions' => 'Разрешения',
    ],
    'permissions' => [
        'allow_all' => 'Позволять все',
        'deny_all' => 'Отрицать все',
        'inherit_all' => 'Inherit all',
        'allow' => 'Разрешить',
        'deny' => 'Отклонить',
        'inherit' => 'Наследовать',
    ],
];
