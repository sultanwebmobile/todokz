<?php

return [
    'users' => [
        'index' => 'Индекс пользователей',
        'create' => 'Создать пользователей',
        'edit' => 'Редактировать пользователей',
        'destroy' => 'Удалить пользователей',
    ],
    'roles' => [
        'index' => 'Индекс роли',
        'create' => 'Создавать роли',
        'edit' => 'Редактировать роли',
        'destroy' => 'Удалить роли',
    ],
];
