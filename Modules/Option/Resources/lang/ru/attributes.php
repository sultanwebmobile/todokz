<?php

return [
    'name' => 'Имя',
    'type' => 'Тип',
    'is_required' => 'Обязательный',
    'label' => 'Метка',
    'price' => 'Цена',
    'price_type' => 'Тип цены',

    // Validations
    'values.*.label' => 'Метка',
    'values.*.price' => 'Цена',
    'values.*.price_type' => 'Тип цены',

    'options.*.name' => 'Name',
    'options.*.type' => 'Тип',
    'options.*.values.*.label' => 'Метка',
    'options.*.values.*.price' => 'Цена',
    'options.*.values.*.price_type' => 'Тип цены',
];
