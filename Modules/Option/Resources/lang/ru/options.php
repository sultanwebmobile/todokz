<?php

return [
    'option' => 'Опция',
    'options' => 'Опции',
    'select_global_option' => 'Выберите глобальный вариант',
    'please_select_a_option_type' => 'Пожалуйста, выберите тип опции.',
    'table' => [
        'name' => 'Имя',
        'type' => 'Тип',
    ],
    'tabs' => [
        'group' => [
            'option_information' => 'Опция информации',
        ],
        'general' => 'Общее',
        'values' => 'Значения',
        'product' => [
            'options' => 'Опции',
        ],
    ],
    'form' => [
        'this_option_is_required' => 'Эта опция требуется',
        'add_new_option' => 'Добавить новую опцию',
        'add_global_option' => 'Добавить глобальную опцию',
        'new_option' => 'Новая опция',
        'option_types' => [
            'please_select' => 'Пожалуйста выберите',
            'text' => 'Текст',
            'field' => 'Поле',
            'textarea' => 'Текстовая область',
            'select' => 'Выбрать',
            'dropdown' => 'Выпадающий',
            'checkbox' => 'Флажок',
            'checkbox_custom' => 'Пользовательский флажок',
            'radio' => 'Переключатель',
            'radio_custom' => 'Настраиваемый переключатель',
            'multiple_select' => 'Выберите несколько опции',
            'date' => 'Дата',
            'date_time' => 'Дата и время',
            'time' => 'Время',
        ],
        'delete_option' => 'Удалить вариант',
        'price' => 'Цена',
        'price_types' => [
            'fixed' => 'Фиксированный',
            'percent' => 'Процентов',
        ],
        'add_new_row' => 'Добавить новую строку',
        'delete_row' => 'Удалить строку',
    ],
];
