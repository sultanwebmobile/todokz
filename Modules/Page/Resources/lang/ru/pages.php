<?php

return [
    'page' => 'Страница',
    'pages' => 'Страницы',
    'table' => [
        'name' => 'Название',
    ],
    'tabs' => [
        'group' => [
            'page_information' => 'Информация о странице',
        ],
        'general' => 'Общее',
        'seo' => 'SEO',
    ],
    'form' => [
        'enable_the_page' => 'Включить страницу',
    ],
];
