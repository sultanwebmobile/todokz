<?php

return [
    'welcome' => "Добро пожаловать :first_name,\nВаша учетная запись была успешно создана.",
    'new_order' => 'Новый заказ был размещен. Идентификатор заказа #:order_id.',
    'order_has_been_placed' => "Привет  :first_name,\nВаш заказ #:order_id был размещен. Спасибо за покупку.",
    'order_status_changed' => "Привет  :first_name,\nВаш заказ #:order_id статус изменен на :status.",
];
