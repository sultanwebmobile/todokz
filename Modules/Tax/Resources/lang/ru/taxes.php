<?php

use Modules\Tax\Entities\TaxClass;

return [
    'tax' => 'Налог',
    'taxes' => 'Налоги',
    'table' => [
        'tax_class' => 'Класс налога',
    ],
    'tabs' => [
        'group' => [
            'tax_information' => 'Информация о налоге',
        ],
        'general' => 'Общее',
        'rates' => 'Рейтинги',
    ],
    'form' => [
        'add_new_rate' => 'Добавить новый рейтинг',
        'delete_rate' => 'Удалить рейтинг',
        'based_on' => [
            TaxClass::SHIPPING_ADDRESS => 'Адрес доставки',
            TaxClass::BILLING_ADDRESS => 'Адрес платежа',
            TaxClass::STORE_ADDRESS => 'Адрес магазина',
        ],
    ],
];
