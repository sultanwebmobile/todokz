<?php

return [
    'product' => 'Продукт',
    'products' => 'Продукты',
    'table' => [
        'thumbnail' => 'Миниатюра',
        'name' => 'Название',
        'price' => 'Цена',
    ],
    'tabs' => [
        'group' => [
            'basic_information' => 'Базовая информация',
            'advanced_information' => 'Подробная информация',
        ],
        'general' => 'Общее',
        'price' => 'Цена',
        'inventory' => 'Инвентарь',
        'images' => 'Изображений',
        'downloads' => 'Загрузки',
        'seo' => 'SEO',
        'related_products' => 'Сопутствующие товары',
        'up_sells' => 'Раскрутки',
        'cross_sells' => 'Кросс продажи',
        'additional' => 'Дополнительный',
    ],
    'form' => [
        'the_product_won\'t_be_shipped' => 'Продукт не будет отправлен',
        'enable_the_product' => 'Включить продукт',
        'price_types' => [
            'fixed' => 'Фиксированный',
            'percent' => 'Процент',
        ],
        'manage_stock_states' => [
            '0' => 'Не отслеживать запасы',
            '1' => 'Отслеживать запасы',
        ],
        'stock_availability_states' => [
            '1' => 'В наличии',
            '0' => 'Нет в наличии',
        ],
        'base_image' => 'Основное изображение',
        'additional_images' => 'Дополнительные изображения',
        'downloadable_files' => 'Загружаемые файлы',
        'file' => 'Файл',
        'choose' => 'Выбрать',
        'delete_file' => 'Удалить файл',
        'add_new_file' => 'Добавить новый файл',
    ],
];
