<?php

return [
    'brand' => 'Бренд',
    'brands' => 'Бренды',
    'table' => [
        'logo' => 'Лого',
        'name' => 'Название',
    ],
    'tabs' => [
        'group' => [
            'brand_information' => 'Информация бренда',
        ],
        'general' => 'Общее',
        'seo' => 'SEO',
        'images' => 'Изображения',
    ],
    'form' => [
        'enable_the_brand' => 'Включить бренд',
        'logo' => 'Лого',
        'banner' => 'Баннер',
    ],
];
