<?php

return [
    'caption_1' => 'Заголовок 2',
    'caption_2' => 'Заголовок 2',
    'direction' => 'Направление',
    'call_to_action_text' => 'Текст призыва к действию',
    'call_to_action_url' => 'URL-адрес вызова действия',
    'open_in_new_window' => 'Открыть в новом окне',
    'caption_1_delay' => 'Задержка',
    'caption_1_effect' => 'Effect',
    'caption_2_delay' => 'Delay',
    'caption_2_effect' => 'Воздействие',
    'call_to_action_delay' => 'Задержка',
    'call_to_action_effect' => 'Воздействие',
    'name' => 'Название',
    'speed' => 'Скорость',
    'autoplay' => 'Автовоспроизведение',
    'autoplay_speed' => 'Скорость автозапуска',
    'fade' => 'Исчезание',
    'dots' => 'Точки',
    'arrows' => 'Стрелки',
];
