<?php

return [
    'slider' => 'Слайд',
    'sliders' => 'Слайды',
    'table' => [
        'name' => 'Название',
    ],
    'tabs' => [
        'group' => [
            'slider_information' => 'Информация о слайдере',
        ],
        'slides' => 'Слайды',
        'settings' => 'Настройки',
    ],
    'slide' => [
        'add_slide' => 'Добавить слайд',
        'image_slide' => 'Изображение слайд',
        'form' => [
            'tabs' => [
                'general' => 'Общее',
                'options' => 'Опции',
            ],
            'directions' => [
                'left' => 'Налево',
                'right' => 'Направо',
            ],
            'caption_1' => 'Заголовок 1',
            'caption_2' => 'Заголовок 2',
            'call_to_action' => 'Призыв к действию',
            'call_to_action_url' => 'Призыв к действию URL',
            'open_in_new_window' => 'Открыть в новом окне',
            '0s' => '0s',
            '0_3s' => '0.3s',
            '0_7s' => '0.7s',
        ],
    ],
    'effects' => [
        'fadeInUp' => 'fadeInUp',
        'fadeInDown' => 'fadeInDown',
        'fadeInLeft' => 'fadeInLeft',
        'fadeInRight' => 'fadeInRight',
        'lightSpeedIn' => 'lightSpeedIn',
        'slideInUp' => 'slideInUp',
        'slideInDown' => 'slideInDown',
        'slideInLeft' => 'slideInLeft',
        'slideInRight' => 'slideInRight',
        'zoomIn' => 'zoomIn',
    ],
    'form' => [
        'enable_autoplay' => 'Включить автозапускy',
        '300ms' => '300ms',
        '3000ms' => '3000ms',
        'use_fade_instead_of_slide' => 'Замирание слайдов вместо скольжения',
        'show_dots' => 'Показать точки ползунка',
        'show_arrows' => 'Показать стрелки Prev/Next',
    ],
];
