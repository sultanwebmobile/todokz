<?php

return [
    'attribute' => 'Атрибут',
    'attributes' => 'Атрибуты',
    'table' => [
        'name' => 'Название',
        'attribute_set' => 'Набор атрибутов',
        'filterable' => 'Фильтруемый',
        'yes' => 'Да',
        'no' => 'Нет',
    ],
    'tabs' => [
        'group' => [
            'attribute_information' => 'Информация о атрибутах',
        ],
        'general' => 'Общее',
        'values' => 'Ценности',
        'product' => [
            'attributes' => 'Атрибуты',
        ],
    ],
    'form' => [
        'use_this_attribute_for_filtering_products' => 'Используйте этот атрибут для фильтрации продуктов',
        'value' => 'Значение',
        'add_new_value' => 'Добавить новое значение',
        'delete_value' => 'Удалить значение',
        'product' => [
            'attribute' => 'Атрибут',
            'values' => 'Стоимости',
            'add_new_attribute' => 'AДобавить новый атрибут',
            'delete_attribute' => 'Удалить атрибут',
        ],
    ],
];
