<?php

return [
    'attribute_set' => 'Набор атрибутов',
    'attribute_sets' => 'Наборы атрибута',
    'table' => [
        'name' => 'Название',
    ],
    'tabs' => [
        'group' => [
            'attribute_set_information' => 'Набор атрибутов информации',
        ],
        'general' => 'Общее',
    ],
];
