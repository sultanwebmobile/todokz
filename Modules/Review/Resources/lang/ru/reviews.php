<?php

return [
    'review' => 'Отзыв',
    'reviews' => 'Отзывы',
    'table' => [
        'product' => 'Продукт',
        'reviewer_name' => 'Имя рецензента',
        'rating' => 'Рейтинг',
        'approved' => 'Утверждено',
    ],
    'tabs' => [
        'group' => [
            'review_information' => 'Обзор информации',
        ],
        'general' => 'общее',
        'products' => [
            'reviews' => 'Отзывы',
        ],
    ],
    'form' => [
        'approve_this_review' => 'Утвердить этот обзор',
    ],
    'shortcuts' => [
        'back_to_product_edit_page' => 'Вернуться к странице редактирования продукта',
    ],
];
