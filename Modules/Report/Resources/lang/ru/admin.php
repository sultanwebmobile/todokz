<?php

return [
    'reports' => 'Отчеты',
    'no_data' => 'Данные недоступны!',
    'filter' => 'Фильтр',
    'filters' => [
        'report_type' => 'Тип отчета',
        'report_types' => [
            'coupons_report' => 'Отчет купонов',
            'customers_order_report' => 'Доклад заказ клиентов',
            'products_purchase_report' => 'Отчет о покупке продуктов',
            'products_stock_report' => 'Продукты запаса отчета',
            'products_view_report' => 'Отчет «Просмотр продуктов»',
            'branded_products_report' => 'Отчет по фирменным продуктам',
            'categorized_products_report' => 'Отчет о продуктах по категориям',
            'taxed_products_report' => 'Отчет о облагаемой налогом продукции',
            'tagged_products_report' => 'Отчет о маркированных продуктах',
            'sales_report' => 'Отчет по продажам ',
            'search_report' => 'Отчет по поиску',
            'shipping_report' => 'Отчет об отгрузке',
            'tax_report' => 'Налоговый отчет ',
        ],
        'date_start' => 'Дата начала',
        'date_end' => 'Дата окончания',
        'group_by' => 'Группа по',
        'groups' => [
            'days' => 'Дней',
            'weeks' => 'Недели',
            'months' => 'Месяцы',
            'years' => 'Годы',
        ],
        'please_select' => 'Пожалуйста выберите',
        'status' => 'Статус заказа',
        'coupon_code' => 'Код купона',
        'customer_name' => 'Имя покупателя',
        'customer_email' => 'Электронная почта клиента',
        'product' => 'Продукт',
        'sku' => 'Единица учета запасов',
        'brand' => 'Бренд',
        'category' => 'Категория',
        'tax_class' => 'Налоговый класс',
        'tag' => 'Тег',
        'keyword' => 'Ключевое слово',
        'quantity_below' => 'Количество ниже',
        'quantity_above' => 'Количество выше',
        'stock_availability' => 'Наличие запасов',
        'stock_availability_states' => [
            'in_stock' => 'В наличии',
            'out_of_stock' => 'Нет в наличии',
        ],
        'shipping_method' => 'Способ доставки',
        'tax_name' => 'Название налога',
    ],
    'table' => [
        'date' => 'Дата',
        'orders' => 'Заказы',
        'products' => 'Продукты',
        'product' => 'Продукт',
        'products_count' => 'Подсчет продуктов',
        'total' => 'Всего',

        // coupons_report
        'coupon_name' => 'Название купона',
        'coupon_code' => 'код купона',

        // customer orders report
        'customer_name' => 'Электронная почта клиента',
        'customer_email' => 'Электронная почта клиента',
        'customer_group' => 'Группа клиентов',
        'guest' => 'Гость',
        'registered' => 'Зарегистрированный',

        // products purchase report
        'qty' => 'Кол-ва',

        // products stock report
        'stock_availability' => 'Наличие запасов',

        // products view report
        'views' => 'Просмотры',

        // branded products report
        'brand' => 'Бренд',

        // category products report
        'category' => 'Категория',

        // taxed products report
        'tax_class' => 'Налоговый класс',

        // tagged products report
        'tag' => 'Тег',

        // sales report
        'subtotal' => 'Промежуточный итог',
        'shipping' => 'Доставка',
        'discount' => 'Скидка',
        'tax' => 'Налог',

        // search report
        'keyword' => 'Ключевое слово',
        'results' => 'Результаты',
        'hits' => 'Хиты',

        // shipping report
        'shipping_method' => 'Способ доставки',

        // tax report
        'tax_name' => 'Название налога',
    ],
];
