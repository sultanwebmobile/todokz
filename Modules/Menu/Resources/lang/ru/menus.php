<?php

return [
    'menu' => 'Меню',
    'menus' => 'Меню',
    'table' => [
        'name' => 'Название',
    ],
    'form' => [
        'please_save_the_menu_first' => 'Пожалуйста, сначала сохраните меню, чтобы добавить пункты меню.',
        'enable_the_menu' => 'Включите меню',
    ],
];
