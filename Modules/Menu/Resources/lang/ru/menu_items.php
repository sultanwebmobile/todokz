<?php

return [
    'menu_item' => 'Пункт меню',
    'back_to_menu_edit_page' => 'Вернуться к меню Редактировать страницу',
    'tabs' => [
        'group' => [
            'menu_item_information' => 'Информация о пункте меню',
        ],
        'general' => 'Общее',
        'image' => 'Изображение',
    ],
    'form' => [
        'types' => [
            'category' => 'Категория',
            'page' => 'Страница',
            'url' => 'URL',
        ],
        'targets' => [
            '_self' => 'Та же вкладка',
            '_blank' => 'Новая вкладка',
        ],
        'select_category' => 'Выберите категорию',
        'select_page' => 'Выберите страницу',
        'select_parent' => 'Выберите родитель',
        'background_image' => 'Фоновая картинка',
        'full_width_menu' => 'Это полное меню ширины',
        'enable_the_menu_item' => 'Включить пункт меню',
    ],
];
