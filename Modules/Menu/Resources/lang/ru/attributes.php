<?php

return [
    'name' => 'Имя',
    'type' => 'Тип',
    'url' => 'URL',
    'icon' => 'Иконка',
    'category_id' => 'Категория',
    'page_id' => 'Страница',
    'parent_id' => 'Пункт родительского меню',
    'target' => 'Таргет',
    'is_fluid' => 'Флуд меню',
    'is_active' => 'Статус',
];
