<?php

return [
    'profile_updated' => 'Ваш профиль обновлен.',
    'default_address_updated' => 'Адрес по умолчанию обновлен.',
    'address_saved' => 'Адрес сохранен.',
    'address_deleted' => 'Адрес удален.',
];
