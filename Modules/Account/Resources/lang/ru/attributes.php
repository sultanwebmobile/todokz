<?php

return [
    'addresses' => [
        'first_name' => 'Имя',
        'last_name' => 'Фамилияe',
        'address_1' => 'Адресная строка',
        'city' => 'Город',
        'zip' => 'Почтовый индекс',
        'country' => 'Страна',
        'state' => 'Область/провинцияe',
    ],
];
