<?php

return [
    'category' => 'Категория',
    'categories' => 'Категории',
    'tree' => [
        'add_root_category' => 'Добавить корневую категорию',
        'add_sub_category' => 'Добавить подкатегорию',
        'collapse_all' => 'Свернуть все',
        'expand_all' => 'Расширить все',
    ],
    'tabs' => [
        'general' => 'Общее',
        'image' => 'Изображение',
        'seo' => 'SEO',
    ],
    'form' => [
        'show_this_category_in_search_box' => 'Показать эту категорию в списке категорий поиска',
        'enable_the_category' => 'Включить категорию',
        'logo' => 'Лого',
        'banner' => 'Баннер',
    ],
];
