<?php

return [
    'id' => 'ID',
    'name' => 'Имя',
    'slug' => 'URL',
    'is_searchable' => 'Доступный для поиска',
    'is_active' => 'Статус',
];
