<?php

return [
    'this_field_is_required' => 'Это поле обязательно к заполнению.',
    'the_selected_option_is_invalid' => 'Выбранный параметр недопустим.',
];
