<?php

return [
    'out_of_stock' => 'Извините, продукт отсутствует на складе.',
    'not_have_enough_quantity_in_stock' => 'Извините, у нас есть только: запас на складе.',
    'one_or_more_product_is_out_of_stock' => 'К сожалению, у одного или нескольких продуктов нет запасов.',
    'one_or_more_product_doesn\'t_have_enough_stock' => 'К сожалению, у одного или нескольких продуктов недостаточно запасов.',
];
