<?php

return [
    'the_importer_has_been_run_successfully' => 'Импортер был успешно запущен.',
    'there_was_an_error_on_rows' => 'Была ошибка на рядах (:rows).',
];
