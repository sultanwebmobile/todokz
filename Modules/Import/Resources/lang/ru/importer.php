<?php

return [
    'importer' => 'Импортер',
    'download_csv' => 'Загрузить CSV',
    'import' => 'Импортировать',
    'import_types' => [
        'product' => 'Продукт',
    ],
    'run' => 'Запустить',
];
