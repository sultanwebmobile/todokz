<?php

return [
    'resource_saved' => ':resource был сохранен.',
    'resource_deleted' => ':resource был удален.',
    'permission_denied' => 'Разрешение запрещено (требуемое разрешение: ":permission").',
];
