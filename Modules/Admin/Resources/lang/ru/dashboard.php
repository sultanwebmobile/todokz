<?php

return [
    'dashboard' => 'Панель управления',
    'total_sales' => 'Тотальная распродажа',
    'total_orders' => 'Общие заказы',
    'total_products' => 'Общая продукция',
    'total_customers' => 'Все клиенты',
    'no_data' => 'Данные недоступны!',
    'latest_search_terms' => 'Последний поиск',
    'latest_orders' => 'Последние заказы',
    'latest_reviews' => 'Последние обзоры',
    'table' => [
        'customer' => 'Клиент',
        'latest_search_terms' => [
            'keyword' => 'Ключевое слово',
            'results' => 'Полученные результаты',
            'hits' => 'Хиты',
        ],
        'latest_orders' => [
            'order_id' => 'ID заказа',
            'status' => 'Статус',
            'total' => 'Всего',
        ],
        'latest_reviews' => [
            'product' => 'Товар',
            'rating' => 'Рейтинг',
        ],
    ],
    'sales_analytics_title' => 'Продажная аналитика',
    'sales_analytics' => [
        'orders' => 'Заказы',
        'sales' => 'Продажи',
        'day_names' => [
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница',
            'Суббота',
            'Воскресенье',
        ],
    ],
];
