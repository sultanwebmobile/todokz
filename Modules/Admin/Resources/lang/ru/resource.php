<?php

return [
    'create' => 'Создать :resource',
    'show' => 'Показать :resource',
    'edit' => 'Редактировать :resource',
    'delete' => 'Удалить :resource',
];
