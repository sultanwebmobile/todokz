<?php

return [
    'content' => 'Содержание',
    'sales' => 'Продажи',
    'system' => 'Система',
    'localization' => 'Локализация',
    'appearance' => 'Внешность',
    'tools' => 'Инструменты',
];
