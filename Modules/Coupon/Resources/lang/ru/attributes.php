<?php

return [
    'name' => 'Название',
    'code' => 'Код',
    'is_percent' => 'Тип скидки',
    'value' => 'Значение',
    'free_shipping' => 'Бесплатная доставка',
    'start_date' => 'Дата начала',
    'end_date' => 'Дата окончания',
    'is_active' => 'Статус',
    'minimum_spend' => 'Минимальное проведение',
    'maximum_spend' => 'Максимальное проведение',
    'products' => 'Товары',
    'exclude_products' => 'Исключать продукты',
    'categories' => 'Категории',
    'exclude_categories' => 'Исключать категории',
    'usage_limit_per_coupon' => 'Ограничение использования на купон',
    'usage_limit_per_customer' => 'Ограничение использования на одного клиента',
];
