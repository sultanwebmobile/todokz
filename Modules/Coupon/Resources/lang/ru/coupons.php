<?php

return [
    'coupon' => 'Купон',
    'coupons' => 'Купоны',
    'table' => [
        'name' => 'Название',
        'code' => 'Код',
        'discount' => 'Скидка',
    ],
    'tabs' => [
        'group' => [
            'coupon_information' => 'Информация купона',
        ],
        'general' => 'Общее',
        'usage_restrictions' => 'Ограничения использования',
        'usage_limits' => 'Ограничения использования',
    ],
    'form' => [
        'price_types' => [
            '0' => 'Фиксированный',
            '1' => 'Процентов',
        ],
        'allow_free_shipping' => 'Разрешить бесплатную доставку',
        'enable_the_coupon' => 'Включить купон',
    ],
];
