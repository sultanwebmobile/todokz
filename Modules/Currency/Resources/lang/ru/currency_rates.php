<?php

return [
    'currency_rate' => 'Курс валюты',
    'currency_rates' => 'Курсы валют',
    'refresh_rates' => 'Обновить цены',
    'refresh_currency_rates_from' => 'Обновить курсы валюты от :service',
    'table' => [
        'currency' => 'Валюта',
        'code' => 'Код',
        'rate' => 'Cтавка ',
        'last_updated' => 'Last Updated',
    ],
    'tabs' => [
        'group' => [
            'currency_rate_information' => 'Currency Rate Information',
        ],
        'general' => 'General',
    ],
];
