<?php

return [
    'out_of_stock' => 'Нет На Складе',
    'new' => 'Новые',
    'add_to_cart' => 'ДОБАВИТЬ В КОРЗИНУ',
    'view_options' => 'ПОСМОТРЕТЬ ВАРИАНТЫ',
    'compare' => 'Сравнить',
    'wishlist' => 'Список желаний',
    'available' => 'Имеется в наличии:',
    'sold' => 'Продано:',
    'years' => 'Годы',
    'months' => 'Месяцы',
    'weeks' => 'Недели',
    'days' => 'Дни',
    'hours' => 'Часы',
    'minutes' => 'Минуты',
    'seconds' => 'Секунды',
];
