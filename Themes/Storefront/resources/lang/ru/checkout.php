<?php

return [
    'checkout' => 'Проверить',
    'account_details' => 'Детали учетной записи',
    'add_new_address' => '+ Добавить новый адрес',
    'billing_details' => 'Платежные реквизиты',
    'you_must_select_an_address' => 'Вы должны выбрать адрес.',
    'please_select' => 'Пожалуйста выберите',
    'create_an_account_by_entering_the_information_below' => 'Создайте учетную запись, введя информацию ниже. Если вы постоянный покупатель, войдите в систему в верхней части страницы.',
    'shipping_details' => 'Детали доставки',
    'special_note_for_delivery' => 'Особое примечание по доставке',
    'payment_method' => 'Метод оплаты',
    'no_payment_method' => 'Способ оплаты не найден.',
    'payment_instructions' => 'Платежные инструкции',
    'i_agree_to_the' => 'Я согласен',
    'terms_&_conditions' => 'Условия и положения',
    'place_order' => 'Разместить заказ',
];
