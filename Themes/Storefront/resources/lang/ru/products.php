<?php

return [
    'browse_categories' => 'Просмотр Категории',
    'price' => 'Цена',
    'filters' => 'Фильтры',
    'latest_products' => 'Последние Продукты',
    'search_results_for' => 'Результаты поиска для:',
    'shop' => 'Магазин',
    'grid_view' => 'Вид Сетки',
    'list_view' => 'Вид Списка',
    'sort_options' => [
        'relevance' => 'Актуальность',
        'alphabetic' => 'По Алфавиту',
        'topRated' => 'Самые популярные',
        'latest' => 'Недавние',
        'priceLowToHigh' => 'Цена: по возрастанию',
        'priceHighToLow' => 'Цена: по убыванию',
    ],
    'per_page_options' => [
        '10' => '10',
        '20' => '20',
        '30' => '30',
        '40' => '40',
        '50' => '50',
    ],
    'show_more' => '+ показать больше',
    'show_less' => '- показать меньше',
    'no_product_found' => 'Ой! Товаров не найдено.',
    'showing_results' => 'Отображение: от-: до из: всего результатов',
];
