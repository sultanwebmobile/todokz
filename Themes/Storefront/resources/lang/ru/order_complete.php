<?php

return [
    'order_placed' => 'Заказ размещен!',
    'your_order_has_been_placed' => 'Ваш заказ был размещен. Идентификатор вашего заказа: <b>:id</b>',
];
