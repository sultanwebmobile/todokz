<?php

return [
    'subject' => 'Счет-фактура по заказу №: id',
    'invoice' => 'ВЫСТАВЛЕННЫЙ СЧЕТ',
    'order_id' => 'Номер заказа',
    'date' => 'Свидание',
    'order_details' => 'Информация для заказа',
    'email' => 'Электронная почта',
    'phone' => 'Телефон',
    'payment_method' => 'Метод оплаты',
    'billing_address' => 'Платежный адрес',
    'shipping_address' => 'Адреса доставки',
    'product' => 'Продукт',
    'unit_price' => 'Цена за единицу',
    'quantity' => 'Количество',
    'line_total' => 'Строка суммы',
    'subtotal' => 'Промежуточный итог',
    'coupon' => 'Купон',
    'total' => 'Итог',
];
