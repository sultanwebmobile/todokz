<?php

return [
    'left_in_stock' => ':количество осталось на складе',
    'in_stock' => 'В наличии',
    'out_of_stock' => 'Нет в наличии',
    'wishlist' => 'Список желаний',
    'compare' => 'Сравнить',
    'options' => [
        'choose_an_option' => 'Выбрать опцию',
    ],
    'quantity' => 'Количество',
    'add_to_cart' => 'Добавить в корзину',
    'sku' => 'Единица складского учета:',
    'categories' => 'Категорий:',
    'tags' => 'Теги:',
    'share' => 'Поделиться:',
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'linkedin' => 'Linkedin',
    'tumblr' => 'Tumblr',
    'you_might_also_like' => 'Вам также может понравиться',
    'description' => 'Описание',
    'specification' => 'Технические характеристики',
    'reviews' => 'Отзывы (:count)',
    'add_a_review' => 'Добавить отзыв',
    'review_form' => [
        'your_rating' => 'Ваш рейтинг',
        'name' => 'Имя',
        'comment' => 'Комментарий',
        'submit' => 'Отправить',
    ],
    'be_the_first_one_to_review_this_product' => 'Оставьте отзыв о товаре первым.',
    'related_products' => 'Последние товары',
];
