<?php

return [
    'compare' => 'Сравнить',
    'no_product' => 'Нет продукта в списке сравнения.',
    'product_overview' => 'Обзор продукта',
    'description' => 'Описание',
    'rating' => 'Оценка',
    'availability' => 'Наличие',
    'in_stock' => 'В наличии',
    'out_of_stock' => 'Нет в наличии',
    'price' => 'Цена',
    'actions' => 'Действия',
    'add_to_cart' => 'Добавить в корзину',
];
