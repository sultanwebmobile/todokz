<?php

return [
    'cart' => 'Корзина',
    'shopping_cart' => 'Корзина покупок',
    'checkout' => 'Проверка',
    'order_complete' => 'Заказ завершен',
    'table' => [
        'image' => 'Изображение',
        'product_name' => 'Название продукта',
        'unit_price' => 'Цена за единицу',
        'quantity' => 'Количество',
        'line_total' => 'Итого строк',
        'unit_price:' => 'Цена за единицу:',
        'quantity:' => 'Количество:',
        'line_total:' => 'Итого строк:',
    ],
    'enter_coupon_code' => 'Введите код купона',
    'apply_coupon' => 'Применить купон',
    'order_summary' => 'Итог заказа',
    'subtotal' => 'Промежуточный итог',
    'coupon' => 'Купон',
    'shipping_method' => 'Способ доставки',
    'total' => 'Итог',
    'proceed_to_checkout' => 'Перейти к оформлению заказа',
    'your_cart_is_empty' => 'Ваша корзина пуста',
    'looks_like_you_haven\'t_made_any_choice_yet' => 'Похоже, вы еще не сделали никакого выбора.',
];
